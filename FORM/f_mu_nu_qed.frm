#-
off statistics;
dimension 3;
cf F(antisymmetric),L,U;
i mu,nu,i,j,k;

l exp = F(U(mu),U(nu))*F(L(mu),L(nu));

print;
.sort

sum mu 0,1;
sum nu 0,2;

print;
.sort

argument;
id L?(1)=L(i);
id L?(2)=L(j);
endargument;

print;

.end