#-
*- has to work together with QED.model from 
*- /afs/ifh.de/group/theorie/Vol1/diana/models/
*#define xi

id Ql=1;

.sort

#ifdef `xi'
#message xi gauge


* `i',1,q,0 --- line number, power of prop, momenta, mass  

  #do i=1,`LINE'
  id VV(`i',mu1?,mu2?,q?,0) = (-d_(mu1,mu2))*Pr(`i',1,q,0)
                +(1-xiA)*q(mu1)*q(mu2)*Pr(`i',2,q,0);
* tr 20 01 2004:
  id FF(`i',num1?,p1?,m?) = (gamma(num1,p1)+m*gameins(num1))*Pr(`i',1,p1,m);
* id FF(`i',num1?,p1?,m?) = (gamma(num1,p1)+m*gi_(1))*Pr(`i',1,p1,m);
* id FF(`i',num1?,p1?,m?) = (gamma(num1,p1)+m*gi_(num1))*Pr(`i',1,p1,m);
* id FF(`i',num1?,p1?,m?) = (gamma(num1,p1)+m*gamma1(num1))*Pr(`i',1,p1,m);
  #enddo

#else

  #do i=1,`LINE'
   id VV(`i',mu1?,mu2?,q?,0) = (-d_(mu1,mu2))*Pr(`i',1,q,0);
* tr 20 01 2004:
* id FF(`i',num1?,p1?,m?) = (gamma(num1,p1)+m*gi_(1))*Pr(`i',1,p1,m);
*  id FF(`i',num1?,p1?,m?) = (gamma(num1,p1)+m*gameins(num1))*Pr(`i',1,p1,m);
 id FF(`i',num1?,p1?,m?) = (gamma(num1,p1)+m*gameins(num1))*Pr(`i',1,p1,m);
  #enddo

#endif

* fermion-(axial)vector-fermion
id F(num1?,num2?,mu1?,GVe?,GAe?,1) =  gamma(num2,mu1)*(GVe + GAe*gamma5(num2));

* fermion-(pseudo)scalar-fermion
id F(num1?,num2?,GVe?,GAe?,0) = GVe + GAe*gamma5(num2);

.sort
