/******************************************************************************
 *                                                                            *
 * file3                                                                      *
 *                                                                            *
 ******************************************************************************/

#ifndef DIAGRAM_HPP
#define DIAGRAM_HPP 1

#include <vector>
#include <utility>
#include <iostream>
#include "Field.hpp"
#include "Vertex.hpp"
#include "Topology.hpp"

using namespace std;

/******************************************************************************
 *                                                                            *
 * LineDescriptor                                                             *
 *                                                                            *
 ******************************************************************************/

struct LineDescriptor
{
  field_pointer _field[2];
};

/******************************************************************************
 *                                                                            *
 * Diagram                                                                    *
 *                                                                            *
 ******************************************************************************/

/// QFT diagram

class Diagram : public Topology
{
public:

  explicit Diagram(const Topology& t);

  pair<field_pointer, field_pointer>
  line(int edge) const;

  vertex_pointer
  vertex(int node) const;

  int
  n_fermion_loops() const;

  vector<vector<int> >
  open_fermion_chains() const;

  vector<vector<int> >
  closed_fermion_chains() const;

  void
  print(ostream& output) const;

  void
  print_info(ostream& output, const char comment_char = '*') const;

private:

  /// these operations can only be performed at the topology level
  void insert_edge();
  void erase_edge();

private:

  void
  determine_sign() const;

  void
  determine_symmetry_factor() const;

  void
  print_vertex(ostream& output, int node, int fermion_chain_index = 0) const;

  void
  print_line(ostream& output, int edge, int fermion_chain_index = 0) const;

  void
  print_momentum(ostream& output,
		 const vector<int>& momentum,
		 const vector<string>& momentum_basis,
		 const string& index = "") const;

  void
  print_fermion_chain(ostream& output,
		      int starting_edge,
		      int fermion_chain_index,
		      vector<bool>& visited_node,
		      vector<bool>& visited_edge) const;

private:

  bool                         _initialized;
  
  mutable int                  _sign;

  mutable bool                 _sign_determined;
  
  mutable int                  _symmetry_factor;

  mutable bool                 _symmetry_factor_determined;

  mutable vector<vector<int> > _open_fermion_chains;

  mutable vector<vector<int> > _closed_fermion_chains;

  vector<vertex_pointer>       _vertex;

  vector<LineDescriptor>       _line;

  friend class DiagramGenerator;
};

/******************************************************************************
 *                                                                            *
 * Inlines of the Diagram class                                               *
 *                                                                            *
 ******************************************************************************/

inline
Diagram::Diagram(const Topology& t) :
  Topology(t),
  _initialized(false),
  _sign(1),
  _sign_determined(false),
  _symmetry_factor(1),
  _symmetry_factor_determined(false),
  _vertex(t.n_nodes()),
  _line(t.n_edges())
{}

inline
pair<field_pointer, field_pointer>
Diagram::line(int edge) const
{
  return make_pair(_line[edge]._field[0], _line[edge]._field[1]);
}

inline
vertex_pointer
Diagram::vertex(int node) const
{
  return _vertex[node];
}

inline
int
Diagram::n_fermion_loops() const
{
  determine_sign();
  return _closed_fermion_chains.size();
}

inline
vector<vector<int> >
Diagram::open_fermion_chains() const
{
  determine_sign();
  return _open_fermion_chains;
}

inline
vector<vector<int> >
Diagram::closed_fermion_chains() const
{
  determine_sign();
  return _closed_fermion_chains;
}

#endif
