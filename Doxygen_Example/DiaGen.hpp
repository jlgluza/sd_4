/******************************************************************************
 *                                                                            *
 * MAIN FILE                                                                  *
 *                                                                            *
 ******************************************************************************/

#ifndef DIAGEN_HPP
#define DIAGEN_HPP 1

#include "Topology.hpp"
#include "TopologyGenerator.hpp"
#include "Diagram.hpp"
#include "DiagramGenerator.hpp"
#include "Model.hpp"

#endif
