x=0.01
s=1.
kmax = 100
tol = 1.e-14
for k in range(kmax):
    print "Przed iteracja %s, s = %s" % (k,s)
#    print "Przed iteracja %s, s = %5.3e" % (k,s)
    s0=s
    s=0.5*(s+x/s)
    delta_s=s-s0
    if abs(delta_s/x) < tol:
        break
print "After %s iterations, s = %s" % (k+1,s) 

"""
gluza@lenovo:~/CWICZ/WSPECJALIST_CAS/PYTHON/sqrt$ python sqrt_convergence.py 
Przed iteracja 0, s = 1.0
Przed iteracja 1, s = 0.505
Przed iteracja 2, s = 0.262400990099
Przed iteracja 3, s = 0.1502553012
Przed iteracja 4, s = 0.10840434673
Przed iteracja 5, s = 0.10032578511
Przed iteracja 6, s = 0.100000528956
Przed iteracja 7, s = 0.100000000001
Przed iteracja 8, s = 0.1
After 9 iterations, s = 0.1
"""
